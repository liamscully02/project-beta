# CarCar

Team:

* Person 1 - Which microservice?
* Matt Bowes - Which microservice? --> Sales

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

I created an Salesperson model, which when adding a sale, the views lists the salesperson. I created a Customer model which allows the user to add a customer to be saved and used for other functions such as viewing what customer bought what automobile and the customer's information(phone number and address). The Sale model keeps track of the automobiles, salesperson, customer and sale price. Salesperson, customer and automobile are individual foreign keys to the Sales model.  A sale can be created when a user inputs the information from the model. All previous sales can be viewed as a list once required information was given. AutomobileVO is the last model that holds the href and vin from automobiles which are then stored into the Inventory API using a poller function.
