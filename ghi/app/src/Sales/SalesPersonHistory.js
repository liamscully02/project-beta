import React, { useState, useEffect } from 'react';

function SalesPersonHistory(){
    const [filterValue, setFilterValue] = useState("")
    const [salesPeople, setSalesPeople] = useState([])

    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
        setSales(data.sales)
    }

    useEffect(() => {
        getData()
    }, [])

    const getSalesPeople = async () => {
        const url = 'http://localhost:8090/api/sales_people/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.sales_person);
        }
    }

    const getSales = async (id) => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
    }

    useEffect(() => {
        getSalesPeople()
        getSales()
    }, [])

    const handleChange = (e) => {
        setFilterValue(e.target.value);
    };

    const filterSales = () => {
        return sales.filter((sales) =>
        sales.salesperson.name.includes(filterValue)
        );
    }

    return (
        <div>
            <h1>Sales Person History</h1>
            <input onChange={handleChange} placeholder='Search a Salesperson' />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Salesperson</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filterSales().map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.vin}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesPersonHistory;
