from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import SalesPerson, Customer, Sales, AutomobileVO
from .encoders import(
    SalesPersonListEncoder,
    CustomerEncoder,
    SalesEncoder,
)


@require_http_methods(["GET", "POST",])
def sales_person_list(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder = SalesPersonListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder = SalesPersonListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create new Sales Person"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST",])
def customer_list(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder = CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe = False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create new Customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def sales_list(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            sales_person = content["salesperson"]
            sales_person = SalesPerson.objects.get(id=content["salesperson"])
            content["salesperson"] = sales_person
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "That sale person does not exist"})
            response.status_code = 404
            return response

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "That customer does not exist"})
            response.status_code = 404
            return response

        sale = Sales.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesEncoder,
            safe=False,
        )
