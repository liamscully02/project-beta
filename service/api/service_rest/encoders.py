from common.json import ModelEncoder
from .models import ServiceAppointment, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number"
    ]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "VIN",
        "customer_name",
        "date",
        "time",
        "technician",
        "reason",
    ]
    encoders = {
        "technician": TechnicianEncoder
    }
