from django.db import models


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()


class ServiceAppointment(models.Model):
    VIN = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=200)
