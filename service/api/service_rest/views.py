import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, ServiceAppointment
from .encoders import ServiceAppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            appointment = ServiceAppointment.objects.create(**content)
            return JsonResponse(appointment,
                                encoder=ServiceAppointmentEncoder,
                                safe=False)
        except:
            return JsonResponse({"message": "Unable to create appointment"}, status=400)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = ServiceAppointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(
                    employee_number=content["technician"]
                )
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"messsage": "Invalid employee number"}, status=400
            )

        ServiceAppointment.objects.filter(id=id).update(**content)

        appointment = ServiceAppointment.objects.get(id=id)
        return JsonResponse(
            appointment, encoder=ServiceAppointmentEncoder, safe=False
        )
